package com.example.ud6_ejemplo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botonAbrir = findViewById(R.id.botonAbrir);

        // Al pulsar el botón "Abrir" creamos nuestro Dialogo (que es un Fragment) y lo mostramos.
        botonAbrir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialogo dialogo = new Dialogo();
                dialogo.show(getSupportFragmentManager(), "DialogoFragment");
            }
        });
    }
}